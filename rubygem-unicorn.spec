%global gem_name unicorn

Summary:       Rack HTTP server for fast clients and Unix
Name:          rubygem-%{gem_name}
Version:       4.8.2
Release:       3%{?dist}
Group:         Development/Languages
License:       GPLv2 or GPLv3 or Ruby
URL:           http://unicorn.bogomips.org/
Source0:       https://rubygems.org/downloads/%{gem_name}-%{version}.gem

%if 0%{?el6}
Requires:      ruby(abi) = 1.8
%else
Requires:      ruby(release)
%endif

Requires:      rubygem(rack)
Requires:      rubygem(raindrops) >= 0.7
Requires:      rubygem(kgio) >= 2.6
BuildRequires: openssl-devel
BuildRequires: ruby-devel
%if 0%{?el6}
BuildRequires: ruby(rubygems)
%else
BuildRequires: rubygems-devel
%endif
BuildRequires: rubygem(rack)
BuildRequires: rubygem(raindrops) >= 0.7
BuildRequires: rubygem(kgio) >= 2.6
BuildRequires: rubygem(minitest)
%if 0%{?fedora} || 0%{?rhel} > 6
BuildRequires: rubygem(isolate)
BuildRequires: ragel
%endif
ExcludeArch:   ppc ppc64
Provides:      rubygem(%{gem_name}) = %{version}

# Do not autorequire shebangs in the -doc subpackage
%global __requires_exclude_from ^%{gem_instdir}/(t|test|script|examples)/.*$

# macros for RHEL6 compatibility:
%{!?gem_dir: %global gem_dir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)}
%{!?gem_instdir: %global gem_instdir %{gem_dir}/gems/%{gem_name}-%{version}}
%{!?gem_libdir: %global gem_libdir %{gem_instdir}/lib}
%{!?gem_cache: %global gem_cache %{gem_dir}/cache/%{gem_name}-%{version}.gem}
%{!?gem_spec: %global gem_spec %{gem_dir}/specifications/%{gem_name}-%{version}.gemspec}
%{!?gem_docdir: %global gem_docdir %{gem_dir}/doc/%{gem_name}-%{version}}
%{!?ruby_sitearch: %global ruby_sitearch %(ruby -rrbconfig -e 'puts Config::CONFIG["sitearchdir"]')}


%description
Unicorn is an HTTP server for Rack applications designed to only serve fast
clients on low-latency, high-bandwidth connections and take advantage of
features in Unix/Unix-like kernels. Slow clients should only be served by
placing a reverse proxy capable of fully buffering both the the request and
response in between Unicorn and slow clients.

%package doc
Summary: Documentation files, rdoc, ri, examples and tests
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

# Fix wrong shebang
sed -i -e '/^#!\//, 1d' bin/unicorn
sed -i -e '/^#!\//, 1d' bin/unicorn_rails
sed -i -e '1i#!/usr/bin/ruby' bin/unicorn
sed -i -e '1i#!/usr/bin/ruby' bin/unicorn_rails

# Require rubygems when running tests
sed -i "2irequire 'rubygems'" test/test_helper.rb

# Require rubygems during binary invocation
sed -i "3irequire 'rubygems'" bin/unicorn
sed -i "3irequire 'rubygems'" bin/unicorn_rails


%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

%if 0%{?el6}
mkdir -p .%{gem_dir}
export CONFIGURE_ARGS="--with-cflags='%{optflags}' $CONFIGURE_ARGS"
gem install \
        -V \
        --local \
        --install-dir .%{gem_dir} \
        --bindir .%{_bindir} \
        --force \
        --rdoc \
        --backtrace \
        %{gem_name}-%{version}.gem
%else
%gem_install
%endif

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

mkdir -p %{buildroot}%{_bindir}
cp -a .%{gem_instdir}/bin/unicorn %{buildroot}%{_bindir}/
cp -a .%{gem_instdir}/bin/unicorn_rails %{buildroot}%{_bindir}/

# Remove leftovers
rm %{buildroot}/%{gem_instdir}/.CHANGELOG.old
rm %{buildroot}/%{gem_instdir}/.document
rm %{buildroot}/%{gem_instdir}/.gitignore
rm %{buildroot}/%{gem_instdir}/.mailmap
rm %{buildroot}/%{gem_instdir}/.manifest
rm %{buildroot}/%{gem_instdir}/GIT-VERSION-FILE
rm %{buildroot}/%{gem_instdir}/GIT-VERSION-GEN
rm %{buildroot}/%{gem_instdir}/GNUmakefile
rm %{buildroot}/%{gem_instdir}/setup.rb
rm %{buildroot}/%{gem_instdir}/.wrongdoc.yml
rm -rf %{buildroot}%{gem_instdir}/ext
find %{buildroot} -name .gitignore -exec rm {} +
rm %{buildroot}/%{gem_instdir}/%{gem_name}.gemspec

# In case you have yard installed
rm -rf %{buildroot}%{gem_instdir}/.yardoc

mkdir -p %{buildroot}%{_mandir}
mv %{buildroot}/%{gem_instdir}/man/* %{buildroot}%{_mandir}/

# Move C extensions to the extdir.

%if 0%{?el6}
  # %%{gem_extdir_mri} doesn't really work on EL 6
  mkdir -p %{buildroot}%{ruby_sitearch}
  # move the extension to ruby_sitearch
  mv %{buildroot}%{gem_libdir}/unicorn_http.so %{buildroot}%{ruby_sitearch}
%else
  install -d -m0755 %{buildroot}%{gem_extdir_mri}/lib
  # move the extension to gem_extdir_mri
  mv %{buildroot}%{gem_libdir}/unicorn_http.so %{buildroot}%{gem_extdir_mri}/lib/
%endif

%check
# Don't use testrb.
# http://rubyforge.org/pipermail/mongrel-unicorn/2012-December/001647.html
pushd .%{gem_instdir}
  %if 0%{?el6}
    # TODO: make tests work on EL6.
  %else
    make test
  %endif
popd

%files
%if 0%{?el6}
%{ruby_sitearch}/unicorn_http.so
%else
%{gem_extdir_mri}
%endif
%dir %{gem_instdir}
%{gem_cache}
%{gem_spec}
%{gem_libdir}
%exclude %{gem_cache}
%{_bindir}/unicorn
%{_bindir}/unicorn_rails
%{gem_instdir}/bin
%doc %{_mandir}/man1/unicorn.1.gz
%doc %{_mandir}/man1/unicorn_rails.1.gz
%doc %{gem_instdir}/LICENSE
%doc %{gem_instdir}/COPYING
%doc %{gem_instdir}/CONTRIBUTORS
%doc %{gem_instdir}/README
%doc %{gem_instdir}/Application_Timeouts
%doc %{gem_instdir}/LATEST
%doc %{gem_instdir}/Links

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/ChangeLog
%doc %{gem_instdir}/FAQ
%doc %{gem_instdir}/DESIGN
%doc %{gem_instdir}/HACKING
%doc %{gem_instdir}/ISSUES
%doc %{gem_instdir}/KNOWN_ISSUES
%doc %{gem_instdir}/NEWS
%doc %{gem_instdir}/PHILOSOPHY
%doc %{gem_instdir}/SIGNALS
%doc %{gem_instdir}/Sandbox
%doc %{gem_instdir}/TODO
%doc %{gem_instdir}/TUNING
%doc %{gem_instdir}/local.mk.sample
%doc %{gem_instdir}/test
%doc %{gem_instdir}/t
%doc %{gem_instdir}/Documentation
%doc %{gem_instdir}/examples
%{gem_instdir}/Rakefile
%{gem_instdir}/script

%changelog
* Thu Mar 13 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 4.8.2-3
- Fixes for Fedora Review Request (RHBZ #1065685)
- convert tabs to spaces
- use %%{gem_libdir}
- include %%{gem_extdir_mri} and %%{gem_instdir}/bin directories
- exclude %%{gem_cache}

* Sat Feb 15 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 4.8.2-2
- Drop Fedora 18 conditionals
- Drop permissions adjustment (fixed upstream)
- Reword comment about extdir

* Mon Feb 10 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 4.8.2-1
- Update to 4.8.2

* Sun Nov 24 2013 Ken Dreyer <ktdreyer@ktdreyer.com> - 4.7.0-1
- Update to 4.7.0

* Mon Aug  5 2013 Guillermo Gómez <gomix@fedoraproject.org> - 4.6.3-1
- Initial package with version 4.6.3
